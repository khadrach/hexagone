resource "aws_ecr_repository" "hexagone" {
  name                 = "hexagone"
  image_tag_mutability = "MUTABLE"
}