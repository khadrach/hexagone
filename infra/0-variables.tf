
variable "repository_branch" {
  description = "Repository branch to connect to"
  default     = "main"
}

variable "repository_owner" {
  description = "GitHub repository owner"
  default     = "khadrach"
}

variable "repository_name" {
  description = "GitHub repository name"
  default     = "test"
}