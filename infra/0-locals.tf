locals {
  name            = "hexagone"
  cluster_version = "1.22"
  region          = "us-east-1"
}

variable "artifacts_bucket_name" {
  description = "S3 Bucket for storing artifacts"
  default     = "hexagone.artifacts"
}