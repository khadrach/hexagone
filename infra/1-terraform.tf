terraform {
  
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14"
    }
    curl = {
      version = "1.0.2"
      source  = "anschoewe/curl"
    }
    
  }
}

provider "aws" {
  region = "us-east-1"
}